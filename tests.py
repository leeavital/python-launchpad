from datastructures import *
import os

song = Song(bpm=200)
song.set_note(0,0,True)
song.set_note(0,2,True)
song.set_note(0,4,True)

song.set_note(1,0,True)
song.set_instrument(1,fileName="loop.aif")
song.set_instrument_pitch(1, 0.2)

song.start()

