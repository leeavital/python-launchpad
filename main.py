import threading
import time

from pyo import *


class GlobalPYOServerInterface:
   

   @staticmethod
   def start():
      GlobalPYOServerInterface.s = Server().boot().start()




class Player( threading.Thread ):

   def __init__( self ):
      threading.Thread.__init__( self )
      self.bpm = 75.0
      self.i  = 0

      GlobalPYOServerInterface.start()

      self.kick = SfPlayer( "Kick_14.wav" )
      self.loop = SfPlayer( "loop.aif" )



   def run( self ):
      while( True ):
         n = self.i % 16

         if n == 0:
            self.loop.out()

         self.loop.mul = self.loop.mul * 1.2

         self.kick.out()
         time.sleep( 1 / (self.bpm / 60.0)  )

         self.i = self.i + 1




p = Player( )
p.start()

      
