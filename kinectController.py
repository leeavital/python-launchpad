pose_to_use = 'Psi'


from openni import *
import math, pygame, SimpleCV
import time

pygame.init()

screen = pygame.display.set_mode((640, 480))
pygame.display.set_caption("Head")


ctx = Context()
ctx.init()

kinectImage = ImageGenerator()
kinectImage.create(ctx)


depth = DepthGenerator()
depth.create(ctx)

# Create the user generator
user = UserGenerator()
user.create(ctx)

skel_cap = user.skeleton_cap
pose_cap = user.pose_detection_cap
t = None
# Declare the callbacks
def new_user(src, id):
    print "1/4 User {} detected. Looking for pose..." .format(id)
    pose_cap.start_detection(pose_to_use, id)

def pose_detected(src, pose, id):
    print "2/4 Detected pose {} on user {}. Requesting calibration..." .format(pose,id)
    pose_cap.stop_detection(id)
    skel_cap.request_calibration(id, True)

def calibration_start(src, id):
    print "3/4 Calibration started for user {}." .format(id)

def calibration_complete(src, id, status):
    if status == CALIBRATION_STATUS_OK:
        print "4/4 User {} calibrated successfully! Starting to track." .format(id)
        skel_cap.start_tracking(id)
    else:
        print "ERR User {} failed to calibrate. Restarting process." .format(id)
        new_user(user, id)

def lost_user(src, id):
    print "--- User {} lost." .format(id)
    # if allUsers[id]['instrument'] == 'theremin':
    #     tmn.stop()
def magnitude(v):
    return math.sqrt(sum(v[i]*v[i] for i in range(len(v))))

def add(u, v):
    return [ u[i]+v[i] for i in range(len(u)) ]

def sub(u, v):
    return [ u[i]-v[i] for i in range(len(u)) ]

def dot(u, v):
    return sum(u[i]*v[i] for i in range(len(u)))

def normalize(v):
    vmag = magnitude(v)
    return [ v[i]/vmag  for i in range(len(v)) ]

def ccw(A,B,C):
    return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])

# Return true if line segments AB and CD 
def contains(box, point):
    #0 = x, 1 = y, 2 = w, 3 = h
    
    return point[0] < (box[0] + box[2]) and point[0] > box[0] and point[1] < (box[1] + box[3]) and point[1] > box[1] 
def intersect(A,B,C,D):
    return ccw(A,C,D) != ccw(B,C,D) and ccw(A,B,C) != ccw(A,B,D)
# Register them
user.register_user_cb(new_user, lost_user)
pose_cap.register_pose_detected_cb(pose_detected)
skel_cap.register_c_start_cb(calibration_start)
skel_cap.register_c_complete_cb(calibration_complete)

# Set the profile
skel_cap.set_profile(SKEL_PROFILE_ALL)

# Start generating
ctx.start_generating_all()
print "0/4 Starting to detect users. Press Ctrl-C to exit."


t = time.time()
enterBoxVal = (-1, 0)
first = True
while True:
    # Update to next frame

    ctx.wait_and_update_all()
    image = kinectImage.get_raw_image_map_bgr()
    newImage = pygame.image.fromstring(image,(640,480),"RGB")
    finalImage = SimpleCV.Image(pygame.surfarray.array2d(newImage)).flipHorizontal()


    finalImage.show()
    boxes = [(100,100,100,100), (210,100,100,100), (100,210,100,100),
             (210,210,100,100)]
    pygame.draw.rect(screen, (255,0,0), boxes[0], 2)

    pygame.draw.rect(screen, (255,0,0), boxes[1], 2)

    pygame.draw.rect(screen, (255,0,0), boxes[2], 2)

    pygame.draw.rect(screen, (255,0,0), boxes[3], 2)


    for id in user.users:

        if skel_cap.is_tracking(id):
            

            RH = skel_cap.get_joint_position(id, SKEL_LEFT_HAND)
            # LH = skel_cap.get_joint_position(id, SKEL_LEFT_HAND)
            #determine key point for instrument
            
            # LHCENTER = LH.point
            RHCENTER = RH.point


            RHnormalX = 640-int(((RH.point[0]+1000)/2000) * 640)
            RHnormalY = 480-int(((RH.point[1]+500)/1000) * 480)
            RHZ = RH.point[2]

            pygame.draw.circle(screen, (255,0,0), (RHnormalX,RHnormalY), 5, 4)
            # LHnormalX = 640-int(((LH.point[0]+1000)/2000) * 640)
            # LHnormalY = 480-int(((LH.point[1]+500)/1000) * 480)
            # pygame.draw.circle(screen, (0,255,0), (LHnormalX,LHnormalY), 5, 4)
            
            for x in boxes:
                if contains(x, (RHnormalX, RHnormalY)):
                    pygame.draw.rect(screen, (0,255,0), x, 2)
                    boxIn = boxes.index(x)
                    if boxIn != enterBoxVal[0]:
                        changed = True
                        enterBoxVal = (boxIn, RHZ)
                    elif RHZ - enterBoxVal[1] < -100:
                        pygame.draw.rect(screen, (0,0,255), x, 2)

                    # print "Hand in box: " + str(boxes.index(x) + 1)

    pygame.display.flip()