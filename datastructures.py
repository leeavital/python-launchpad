from pyo import *
import threading

class GlobalServerInterface():
    @staticmethod
    def start():
        GlobalServerInterface.server = Server().boot().start()

class Song(threading.Thread):
    def __init__(self, bpm=100, tracks=5, length=10 ):
        self.lock = threading.Lock()
        self.lock.acquire()
        threading.Thread.__init__(self)
        GlobalServerInterface.start()
        self.bpm = bpm
        self.length = length
        self.time = 0
        self.instruments = [Instrument(length) for i in range(tracks)]
        self.lock.release()
    def set_instrument(self,instrument=0,fileName="Kick_14.wav"):
        self.lock.acquire()
        self.instruments[instrument].load_file(fileName)
        self.lock.release()
    def set_note(self,instrument=0,note=0,active=False):
        self.lock.acquire()
        self.instruments[instrument].notes[note] = active
        self.lock.release()

    def set_instrument_amplitude( self, instrument, mult ):
        self.lock.acquire()
        self.instruments[ instrument ].player.mul = mult
        self.lock.release()

    def set_instrument_pitch(self, instrument, pitch):
        self.lock.acquire()
        self.instruments[instrument].player.speed = pitch
        self.lock.release()

    def load_instrument( self, channel_num, load_file, beats=1 ):
       self.set_instrument( channel_num, load_file )
       for i in range( self.length  ):
          if i % beats == 0:
             self.set_note( channel_num, i , True )
          else:
             self.set_note( channel_num, i, False )

    def enable_instrument( self, channel_num ):
       for i in range( 0, self.length ):
          self.set_note( channel_num, i, True )

    def disable_instrument( self, channel_num ):
       for i in range( 0, self.length ):
          self.set_note( channel_num, i, False )

    def start_playback( self ):
       self.start()

    def run(self):
        while(True):
            self.lock.acquire()
            for instrument in self.instruments:
                instrument.play(self.time)
            self.time = (self.time + 1) % self.length
            self.lock.release()
            time.sleep(1 / (self.bpm / 60.0))

class Instrument():
    def __init__(self, length=16, fileName="Kick_14.wav"):
        self.load_file(fileName)
        self.notes = [False for i in range(length)]
    def load_file(self, fileName="Kick_14.wav"):
        self.player = SfPlayer(fileName)
    def play(self, time):
        if self.notes[time]:
            self.player.out()
