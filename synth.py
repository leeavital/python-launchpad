from pyo import *
import threading


import datastructures

datastructures.GlobalServerInterface.start()


class Synthesizer( threading.Thread ):
    """parameters: LFO, Pitch"""


    def __init__( self ):
        threading.Thread.__init__( self ) 
        self.lfo = Osc( table=CosTable(), freq=100000  )
        self.synth = Osc(  table=SawTable(), freq=100, mul=self.lfo )

    def run( self ):
        self.synth.out()

        while( True ):
            pass
    

    @property
    def  freq( self ):
        return self.synth.freq

    @freq.setter
    def freq( self, freq ):
        self.synth.freq = freq
    
    @property
    def LFOFreq( self ):
        return self.lfo.freq

    @LFOFreq.setter
    def LFOFreq( self, freq ):
        self.lfo.freq = freq





# while( True ):
#     i = input( "quit?" )
#     if i == "y":
#         break
# s.stop()


    


     
    


